#!/usr/bin/env lua

local function get_uptime()
    local os_name = package.config:sub(1, 1) == '\\' and 'windows' or 'linux'
    local uptime_seconds
    if os_name == 'windows' then
        local ffi = select(2, pcall(require, 'ffi'))
        if type(ffi) == 'table' then
            ffi.cdef [==[
            typedef unsigned long DWORD;
            DWORD GetTickCount();
            ]==]
            uptime_seconds = tonumber(ffi.C.GetTickCount()) / 1000
        else
            print('ERR: LuaJIT is required to use this command.')
        end
    else
        local f = io.open('/proc/uptime')
        if f then
            local line = f:read('*line')
            f:close()
            uptime_seconds = tonumber(line:match('([%d.]+)'))
        end
    end
    if type(uptime_seconds) == 'number' then
        uptime_seconds = math.floor(uptime_seconds)
        return uptime_seconds
    end
end

local function format_time(seconds)
    local days = math.floor(seconds / (24 * 3600))
    seconds = seconds % (24 * 3600)
    local hours = math.floor(seconds / 3600)
    seconds = seconds % 3600
    local minutes = math.floor(seconds / 60)
    seconds = seconds % 60
    return days, hours, minutes, seconds
end

local uptime = get_uptime()
if type(uptime) == 'number' then
    local days, hours, minutes, seconds = format_time(uptime)
    local formatted_time = string.format('%d days, %d hours, %d minutes, %d seconds', days, hours, minutes, seconds)
    print('System Uptime: ' .. formatted_time)
else
    print('ERR: Could not retrieve the system uptime.')
end
