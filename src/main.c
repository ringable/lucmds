#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef _WIN32
#include <windows.h>
#define LUA_SCRIPT_DIR "..\\scripts\\"
#else
#include <unistd.h>
#define LUA_SCRIPT_DIR "../scripts/"
#endif

#define LUA_SCRIPT_EXT ".lua"
#define CONFIG_FILE "config.txt"

char lua_interpreter[256] = "luajit"; // Default interpreter

void read_config()
{
	FILE *config = fopen(CONFIG_FILE, "r");
	if (config == NULL)
	{
		printf("Configuratoin file not found. Using default Lua interpreter.\n");
		return;
	};

	char line[256];
	while (fgets(line, sizeof(line), config))
	{
		if (strncmp(line, "lua_interpreter=", 16) == 0)
		{
			strncpy(lua_interpreter, line + 16, sizeof(lua_interpreter) - 1);
			lua_interpreter[strcspn(lua_interpreter, "\n")] = 0; // remove newline char
		}
	}

	fclose(config);
}

void run_lua_script(const char *script_name)
{
	char command[256];
	#ifdef _WIN32
		snprintf(command, sizeof(command), "\"%s\" %s%s", lua_interpreter, LUA_SCRIPT_DIR, script_name);
	#else
		snprintf(command, sizeof(command), "%s %s%s", lua_interpreter, LUA_SCRIPT_DIR, script_name);
	#endif

	FILE *fp = popen(command, "r");
	if (fp == NULL)
	{
		printf("No such module found.\n");
		exit(1);
	}

	char buffer[512];
	while (fgets(buffer, sizeof(buffer), fp) != NULL)
	{
		printf("%s", buffer);
	}

	pclose(fp);
}

int main(int argc, char *argv[])
{
	if (argc != 2)
	{
		printf("Usage: %s <module>\n", argv[0]);
		return 1;
	}

	char *script_name = argv[1];

	if (strstr(script_name, LUA_SCRIPT_EXT) == NULL)
	{
		char *full_script_name = malloc(strlen(script_name) + strlen(LUA_SCRIPT_EXT) + 1);
		strcpy(full_script_name, script_name);
		strcat(full_script_name, LUA_SCRIPT_EXT);
		run_lua_script(full_script_name);
		free(full_script_name);
	}
	else
	{
		run_lua_script(script_name);
	}

	return 0;
}