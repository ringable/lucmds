#!/bin/bash

echo "Building the project..."
gcc -o "bin/x" "src/main.c"
if [ $? -eq 0 ]; then
    echo "Build successful."
else
    echo "Build failed."
fi
echo "Building process completed."
