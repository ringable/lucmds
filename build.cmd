@echo off
echo Building the project...
gcc -o "bin\x.exe" "src\main.c"
if %errorlevel% equ 0 (
    echo Build successful.
) else (
    echo Build failed.
)
echo Building process completed.
